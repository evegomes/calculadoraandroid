package br.edu.ifpr.webandroid.calculadora;

/**
 * Created by everaldo on 11/12/16.
 */
public class ZeroDivisionException extends Throwable {
    public ZeroDivisionException(String message) {
        super(message);
    }
}
