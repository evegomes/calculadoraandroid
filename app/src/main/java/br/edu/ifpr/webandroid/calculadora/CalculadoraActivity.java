package br.edu.ifpr.webandroid.calculadora;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    public static final String OPERANDO1 = "OPERANDO1";
    public static final String OPERANDO2 = "OPERANDO2";
    public static final String OPERACAO = "OPERACAO";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
    }


    public void calcular(View view){
        Intent intent = new Intent(this, ResultadoActivity.class);
        EditText operando1 = (EditText) findViewById(R.id.operando1);
        EditText operando2 = (EditText) findViewById(R.id.operando2);
        Spinner operacoes = (Spinner) findViewById(R.id.operacoes);
        String operacao = operacoes.getSelectedItem().toString();

        if("".equals(operando1.getText().toString()) ||
                "".equals(operando2.getText().toString())){
            Toast.makeText(this, "Preencha os dois operandos", Toast.LENGTH_SHORT).show();
        }
        else{
            intent.putExtra(OPERANDO1, operando1.getText().toString());
            intent.putExtra(OPERANDO2, operando2.getText().toString());
            intent.putExtra(OPERACAO, operacao);
            startActivity(intent);
        }




    }
}
