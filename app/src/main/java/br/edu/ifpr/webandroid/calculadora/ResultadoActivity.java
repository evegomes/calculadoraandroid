package br.edu.ifpr.webandroid.calculadora;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ResultadoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        Intent intent = getIntent();
        Double operando1 = Double.parseDouble(intent.getStringExtra(CalculadoraActivity.OPERANDO1));
        Double operando2 = Double.parseDouble(intent.getStringExtra(CalculadoraActivity.OPERANDO2));
        String operacao = intent.getStringExtra(CalculadoraActivity.OPERACAO);


        Log.d("Calculadora", operando1 + " " + operacao + " " + operando2);

        TextView resultado = (TextView) findViewById(R.id.resultado);

        try {
            double calc = Calculadora.calcular(operando1, operando2, operacao);
            resultado.setText(operando1 + " " + operacao + " " + operando2 + "= " + calc);
        } catch (ZeroDivisionException e) {
            resultado.setText(e.getMessage());
        }

    }

    public void telaInicial(View view){
        Intent intent = new Intent(this, CalculadoraActivity.class);
        startActivity(intent);
    }
}
