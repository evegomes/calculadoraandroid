package br.edu.ifpr.webandroid.calculadora;

/**
 * Created by everaldo on 11/12/16.
 */

public class Calculadora {

    private static final String SUBTRACAO = "Subtração";
    private static final String SOMA = "Soma";
    private static final String MULTIPLICACAO = "Multiplicação";
    private static final String DIVISAO = "Divisão";

    public static double calcular(Double operando1, Double operando2, String operacao) throws ZeroDivisionException {
        if(SOMA.equals(operacao)){
            return operando1 + operando2;
        }
        else if(SUBTRACAO.equals(operacao)){
            return operando1 - operando2;
        }
        else if(MULTIPLICACAO.equals(operacao)){
            return operando1 * operando2;
        }
        else if(DIVISAO.equals(operacao)){
            if(operando2 == 0){
                throw new ZeroDivisionException("Impossível dividir por zero");
            }
            return operando1 / operando2;
        }
        return 0f;
    }

}
